import './public-path';
import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import App from './App.vue';
import routes from './router';
import store from './store';
import userApi from './api/userApi';

// 按需导入element-ui
// import "./plugins/element.js";

let router = null;
var instance = createApp(App);
let history = null;
import 'element-plus/lib/theme-chalk/index.css'
import ElementPlus from 'element-plus'


let componentsList = [];
let _pager = '';
let _utils = '';


/**
 * bootstrap 只会在微应用初始化的时候调用一次，下次微应用重新进入时会直接调用 mount 钩子，不会再重复触发 bootstrap。
 * 通常我们可以在这里做一些全局变量的初始化，比如不会在 unmount 阶段被销毁的应用级别的缓存等。
 */

export async function bootstrap({ components,pager,utils,http}) {
  console.log('vue3----bootstrap',components,pager,utils,http)
  // 注册主应用下发的组件
  //  Vue.component(components);
  componentsList = components;
  instance.component(components);
  // // 把工具函数挂载在vue $mainUtils对象
  // Vue.prototype.$mainUtils = utils;

  // // 把mainEmit函数一一挂载
  // Object.keys(emitFnc).forEach(i => {
  //     Vue.prototype[i] = emitFnc[i]
  // });

  instance.config.globalProperties.$utils = utils;
  _utils = utils
  instance.config.globalProperties.$pager = pager;
  instance.config.globalProperties.$http = http;
  userApi.serIntance(http);
  _pager = pager
  // 在子应用注册呼机
  pager.subscribe(v => {
    console.log(`vue3监听到子应用${v.from}发来消息：`, v)
    // store.dispatch('app/setToken', v.token)
  })
  // Vue.prototype.$pager = pager;
  // // 在子应用注册官方通信
  // /* actions.onGlobalStateChange((state, prev) => console.log(`子应用subapp-ui监听到来自${state.from}发来消息：`, state, prev)); */
  // Vue.prototype.$actions = actions;
}


function render(props = {}) {
  console.log('应用1render',componentsList,instance,_pager)
  if(!instance){
    instance = createApp(App);
  }
  if(_pager){
    instance.config.globalProperties.$pager = _pager;
  }
  if(_utils){
    instance.config.globalProperties.$utils = _utils;
  }
  const { container } = props;
  history = createWebHistory(window.__POWERED_BY_QIANKUN__ ? '/vue3' : '/');
  router = createRouter({
    history,
    routes,
  });

  instance.use(router);
  instance.use(store);
  // instance.use(ElementPlus);
  instance.use(ElementPlus);
  instance.mount(container ? container.querySelector('#app') : '#app');
}

if (!window.__POWERED_BY_QIANKUN__) {
  render();
}


function storeTest(props) {
  props.onGlobalStateChange &&
    props.onGlobalStateChange(
      (value, prev) => console.log(`[onGlobalStateChange - ${props.name}]:`, value, prev),
      true,
    );
  props.setGlobalState &&
    props.setGlobalState({
      ignore: props.name,
      user: {
        name: props.name,
      },
    });
}

/**
 * 应用每次进入都会调用 mount 方法，通常我们在这里触发应用的渲染方法
 */
export async function mount(props) {
  storeTest(props);
  render(props);
  console.log('vue3mount==',instance)
  instance.config.globalProperties.$onGlobalStateChange = props.onGlobalStateChange;
  instance.config.globalProperties.$setGlobalState = props.setGlobalState;
  // return new Promise((resolve, reject) => {
  //   // Always reject with an Error.
  //   reject(new Error('hi'));
  // });
}

export async function unmount() {
  console.log('vue3->unmount项目被卸载',instance)
  instance.unmount();
  instance._container.innerHTML = '';
  instance = null;
  router = null;
  history.destroy();
}
