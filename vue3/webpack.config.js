const path = require('path');
const htmlWebpackplugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
    entry: path.join(__dirname,'./src/main.js'),
    output: {
        path: path.join(__dirname, './dist'),
        filename: 'bundle.js'
    },
    plugins: [
        new htmlWebpackplugin({ //创建一个在内存中生成的html页面的插件
            template: path.join(__dirname, './src/index.html'),
            filename: 'index.html'
        }),
        new VueLoaderPlugin()
    ],
    module: { //这个节点用于配置所有的第三方模块加载器
        rules: [
            {test: /\.css$/, use:['style-loader','css-loader']},//配置处理.css文件的第三方处理规则
            {test: /\.less$/, use: ["style-loader",'css-loader','less-loader']},
            {test: /\.scss$/, use: ["style-loader",'css-loader','sass-loader']},
            {test: /\.(jpg|png|gif|bmp|jpeg)$/, use: "url-loader?limit=8000"},
            {test: /\.(tff|eot|svg|woff|woff2)$/, use: "url-loader"},
            {test:/\.js$/, use:'babel-loader',exclude:/node_modules/},
            {test: /\.vue$/, use: 'vue-loader'}
        ]
    }
};
