const routes = [
  { path: '/', name: 'home', component: () => import(/* webpackChunkName: "home" */ '@/views/Home') },
  { path: '/about', name: 'about', component: () => import(/* webpackChunkName: "about" */ '@/views/About') },
  { path: '/workerList', name: 'workList', component: () => import(/* webpackChunkName: "about" */ '@/views/workList') },

];

export default routes;
