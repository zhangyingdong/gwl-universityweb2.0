import './public-path';
import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import App from './App.vue';
import routes from './router';
import store from './store';

let router = null;
var instance = createApp(App);
let history = null;


let componentsList = [];
let _pager = '';
export async function bootstrap({ components,pager,utils}) {
  console.log('worker----bootstrap',components,pager,utils)
  // 注册主应用下发的组件
  //  Vue.component(components);
  componentsList = components;
  instance.component(components);
  // // 把工具函数挂载在vue $mainUtils对象
  // Vue.prototype.$mainUtils = utils;

  // // 把mainEmit函数一一挂载
  // Object.keys(emitFnc).forEach(i => {
  //     Vue.prototype[i] = emitFnc[i]
  // });

  instance.config.globalProperties.$utils = utils;
  instance.config.globalProperties.$pager = pager;
  _pager = pager
  // 在子应用注册呼机
  pager.subscribe(v => {
    console.log(`worker监听到子应用${v.from}发来消息：`, v)
    // store.dispatch('app/setToken', v.token)
  })
  // Vue.prototype.$pager = pager;
  // // 在子应用注册官方通信
  // /* actions.onGlobalStateChange((state, prev) => console.log(`子应用subapp-ui监听到来自${state.from}发来消息：`, state, prev)); */
  // Vue.prototype.$actions = actions;
}


function render(props = {}) {
  console.log('worker-render页面开始挂载渲染',componentsList,instance,_pager)
  if(!instance){
    instance = createApp(App);
  }
  if(_pager){
    instance.config.globalProperties.$pager = _pager;

  }
  const { container } = props;
  history = createWebHistory(window.__POWERED_BY_QIANKUN__ ? '/worker' : '/');
  router = createRouter({
    history,
    routes,
  });



  // for (let key in componentsList) {
  //   let item = componentsList[key];
  //   console.log('componentsList',item);
  //
  //   instance.component(item.name,item)
  //
  //
  // }

  instance.use(router);
  instance.use(store);
  instance.mount(container ? container.querySelector('#app') : '#app');
}

if (!window.__POWERED_BY_QIANKUN__) {
  render();
}


function storeTest(props) {
  props.onGlobalStateChange &&
    props.onGlobalStateChange(
      (value, prev) => console.log(`[onGlobalStateChange - ${props.name}]:`, value, prev),
      true,
    );
  props.setGlobalState &&
    props.setGlobalState({
      ignore: props.name,
      user: {
        name: props.name,
      },
    });
}

export async function mount(props) {
  storeTest(props);
  render(props);
  instance.config.globalProperties.$onGlobalStateChange = props.onGlobalStateChange;
  instance.config.globalProperties.$setGlobalState = props.setGlobalState;
}

export async function unmount() {
  console.log('worker被卸载')
  instance.unmount();
  instance._container.innerHTML = '';
  instance = null;
  router = null;
  history.destroy();
}
