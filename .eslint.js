module.exports = {
    root: true,
    parserOptions: {
        sourceType: 'module'
    },
    parser: "vue-eslint-parser",
    env: {
        browser: true,
        node: true,
        es6: true,
    },
    rules: {
      'no-console': 'off',
      "no-irregular-whitespace":"off",//这禁止掉 空格报错检查
      "parser": "babel-eslint",
      "no-unused-vars": 'off',
      "no-unused-expressions":  'off',//禁止无用的表达式
      "no-multiple-empty-lines": [0, {"max": 100}], //空行最多不能超过100行
      'indent': 'off'
    }
}
