import { createRouter, createWebHistory, RouteRecordRaw,createWebHashHistory} from 'vue-router'

const HH = require(/* webpackChunkName: "about" */ '../views/Home.vue')
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: () => require(/* webpackChunkName: "about" */ '../views/Home.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => require(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]
export default routes
