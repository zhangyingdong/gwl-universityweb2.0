import { createApp } from 'vue'
// @ts-ignore
import App from './App.vue'

import routes from './router'
import commonApi from './api/common'
import BaseUtil from './public/baseUtil'
import './index.css'
import {createRouter, createWebHashHistory, createWebHistory} from 'vue-router';

const { name } = require('../package');

import 'element-plus/lib/theme-chalk/index.css'

import ElementPlus from 'element-plus'
const app = createApp(App);


let router = null;
var instance:any = createApp(App);
let history:any = null;

let _window:any = window;

export async function bootstrap({ components,pager,utils,http}) {
  console.log(name+'：微应用初次被构造--bootstrap',components,pager,utils,http)

  // 在子应用注册呼机
  pager.subscribe(v => {
    console.log(name+`:监听到子应用${v.from}发来消息：`, v)
  })
}


function render(props:any) {
  console.log(name+'：render',_window.__POWERED_BY_QIANKUN__,routes);

  if(!instance){
    instance = createApp(App);
  }
  const { container } = props;

  if(props){
    const {pager,utils,http,components} = props;
    console.log(name+'：微应用接收基座的数据',pager,utils,http,components,_window,routes)
    if(pager){
      instance.config.globalProperties.$pager = pager;
    }
    if(http){
      instance.config.globalProperties.$http = http;
      commonApi.serIntance(http);
    }
    if(utils){
      instance.config.globalProperties.$utils = utils;
      BaseUtil.init(utils)
    }
  }
  console.log(name+'：微应用接收基座的数据',111111);

  history = createWebHistory(_window.__POWERED_BY_QIANKUN__ ? '/home/' : '/');
  // history = createWebHistory(_window.__POWERED_BY_QIANKUN__ ? '/home' : '/');
  let rou = createRouter({
    history,
    routes
  });

  console.log(name+'：微应用接收基座的数据',2222222,history);

  instance.use(rou);
  instance.use(ElementPlus);
  instance.mount(container ? container.querySelector('#app') : '#app');
  console.log(name+'：微应用接收基座的数据',33333333,instance)

}

if (!_window.__POWERED_BY_QIANKUN__) {
  console.log(name+'项目单独加载')
  render({});
}

/**
 * 应用每次进入都会调用 mount 方法，通常我们在这里触发应用的渲染方法
 */
export async function mount(props) {
  console.log(name+'：微应用开始渲染',props)

  render(props);
  instance.config.globalProperties.$onGlobalStateChange = props.onGlobalStateChange;
  instance.config.globalProperties.$setGlobalState = props.setGlobalState;
}

export async function unmount() {
  console.log(name+'：微项目被卸载',instance)
  instance.unmount();
  instance._container.innerHTML = '';
  instance = null;
  router = null;
  history.destroy();
}
