
export function getPlatform(): any {

  const ua:any = navigator.userAgent.toLowerCase();
  // 企业微信
  const isWXWork = ua.match(/wxwork/i) == 'wxwork';
  // 微信浏览器
  const isWeixin = !isWXWork && ua.match(/MicroMessenger/i) == 'micromessenger';
  let isMobile = false;
  let isDesktop = false;

  let jsonString = JSON.stringify(ua);
  let wexinMiniLabel = "miniprogram";

  if (navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|IEMobile)/i)) {
    isMobile = true
  } else {
    isDesktop = true
  }
  let platform = '';
  if (isDesktop) {
    console.log('getPlatform==== 电脑端浏览器')
    platform = 'pc'
  } else if (isWeixin) {
    console.log('getPlatform==== 微信浏览器')
    platform = 'wx'
  }else {
    console.log('getPlatform====移动端浏览器')
    platform = 'mobile'
  }
  if(jsonString.indexOf(wexinMiniLabel)>=0){
    platform = 'wxmini'
  }
  return platform;
}
