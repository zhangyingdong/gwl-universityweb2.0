// const baseInfo = {
//   baseUrl: 'https://webapi.goworkla.cn/',
//   agentUrl: 'https://agent.goworkla.cn/'
//  }
const baseInfo = {
  baseUrl: 'http://webapi2.test.goworkla.cn/',
  agentUrl: 'http://agent.test.goworkla.cn/',
}
// const baseInfo = {
//   baseUrl: 'http://192.168.0.120:801/',
//   agentUrl: 'http://192.168.0.120:802/',
// }

// public api = 'http://webapi2.test.goworkla.cn/';          //http请求前缀
// public agentApi = 'http://agent.test.goworkla.cn/';     //http请求前缀
// 预览
// const baseInfo = {
//  baseUrl:"https://apitest.goworkla.cn",
//  agentUrl: "https://agentest.goworkla.cn",
// }

// const baseInfo = {
//  baseUrl:"http://192.168.0.83:8083",
//  agentUrl: "http://192.168.0.83:8082",
// }

// const baseInfo = {
//  baseUrl:"https://webapi.goworkla.cn/",
//  agentUrl: "https://agent.goworkla.cn/",
// }
export default baseInfo
