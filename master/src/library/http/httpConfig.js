import axios from 'axios'
import baseInfo from './baseUrl'
import clientId from './client'
import {Loading, Message} from 'element-ui'
import qs from 'qs';
import CryptoJS from './aes';
import Router from 'vue-router'
// import Vue from '../main.js'
// import VueNot from '../tool/VueNot'

const http = {};

//JS加密
function convertDate (date) {
  let res = date.getFullYear() + ',' + (date.getMonth() + 1) + ',' + date.getDate() + ',' + date.getHours() + ',' + date.getMinutes() + ',' + date.getSeconds();
  return res;
}
function encryp (clientId) {
  let data = clientId + '--' + convertDate(new Date());
  let key = CryptoJS.enc.Utf8.parse('607490BE-18CA-43D7-B11A-57E2621B');
  let iv = CryptoJS.enc.Utf8.parse('2D59831C-78AC-4227-B3F3-CE656636');

  let encrypted = CryptoJS.AES.encrypt(data, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7
  });
  return encrypted.ciphertext.toString( CryptoJS.enc.Base64);
}
var instance = axios.create({
  timeout: 5000,
  baseURL: baseInfo.baseUrl
})
var instanceAgent = axios.create({
  timeout: 5000,
  baseURL: baseInfo.agentUrl
})
// 添加请求拦截器
instance.interceptors.request.use(
  function(config) {
    // 请求头添加token
    // console.log('拦截器1',config)
    if (localStorage.getItem('accesstoken')) {
      let accesstoken = localStorage.getItem('accesstoken')
      config.headers.access_token = accesstoken
    }
    if (localStorage.getItem('accountid')) {
      config.headers.accountid = localStorage.getItem('accountid')
    }
    // console.log('拦截器2')
    config.headers.client_id = clientId
    return config
  },
  function(error) {
    return Promise.reject(error)
  }
)
instanceAgent.interceptors.request.use(
  function(config) {
    config.headers.client_id = encryp(clientId)
    return config
  },
  function(error) {
    return Promise.reject(error)
  }
)
// 响应拦截器即异常处理
instance.interceptors.response.use(
  response => {
    return response.data
  },
  err => {
    if (!(err && err.response)) {
      err.message = '连接服务器失败'
    }
    return Promise.reject(err.response)
  }
)
instanceAgent.interceptors.response.use(
  response => {
    return response.data
  },
  err => {
    if (!(err && err.response)) {
      err.message = '连接服务器失败'
    }
    return Promise.reject(err.response)
  }
)
http.get = function(url, options) {
  let loadingInstance = Loading.service({ fullscreen: true,text: '请求中...' });
  if (options) {
    let paramsArray = [];
    Object.keys(options).forEach(key => paramsArray.push(key + '=' + encodeURIComponent(options[key])))
    if (url.search(/\?/) === -1) {
      url += '?' + paramsArray.join('&')
    } else {
      url += '&' + paramsArray.join('&')
    }
  }
  console.log('http.get', url, options);

  return new Promise((resolve, reject) => {
    instance
      .get(url, options)
      .then(response => {
        console.log('response', response)
        if (response) {
          resolve(response)
        } else {
          // response = { code: 1 }
          resolve(response)
        }
       let time = setTimeout(()=>{
          loadingInstance.close()
            clearTimeout(time)
        },300)
      })
      .catch(e => {
        loadingInstance.close()
        console.log('get_err',e);
        if (e === undefined) {

        } else if (e.data) {
           if (!handleError(e)) {
             return false
           }

          reject(e.data)
        }
      })
  })
}
http.post = function(url, data, options) {
  console.log('post',url,data, options)
  let loadingInstance;
  if (checkisShowLoading(url)){
    loadingInstance = Loading.service({ fullscreen: true, text:'请求中...'});
  }
  return new Promise((resolve, reject) => {
    instance
      .post(url, data, options)
      .then(response => {
        console.log('post 请求成功',response)
        let time = setTimeout(() => {
          if (loadingInstance) {
            loadingInstance.close()
          }
          clearTimeout(time)
        }, 300)
        if (response) {
          resolve(response)
        }else{
          // response = { code: 1 }
          resolve(response)
        }
      })
      .catch(e => {
        if (loadingInstance) {
          loadingInstance.close()
        }
        handleError(e)
        if (e.data) {
          if (!handleError(e)) {
            return false
          }

          reject(e.data)
        }
        console.log('post_err',e)
      })
  })
}
http.agentPost = function(url, data, options) {
  console.log('agentPost')

  let loadingInstance = Loading.service({ fullscreen: true, text: '请求中...' });
  let headers = {};

  if (localStorage.getItem('tokenInfo')) {
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'))
    headers.access_token = tokenInfo.access_token
  }
  if (localStorage.getItem('accountid')) {
    headers.accountid = localStorage.getItem('accountid')
  }
  headers.client_id = encryp(clientId)
  // axios.post(baseInfo.agentUrl + url, qs.stringify(data),{headers:headers})
  //   .then(response => {
  //     console.log('response', response)
  //   },err => {
  //     console.log('err', err)
  //   })


  return new Promise((resolve, reject) => {
    instanceAgent
      .post(url, qs.stringify(data), { })
      .then(response => {
        console.log('agentPostresponse',response)

        loadingInstance.close()
        if (response) {
          resolve(response)
        } else {
          // response = { code: 1 }
          resolve(response)
        }
      })
      .catch(e => {
        console.log('agentPost', e)
        loadingInstance.close()
        if (e.data) {
          if (!handleError(e)) {
            return false
          }
          reject(e.data)
        }
      })
  })
}

function handleError (e) {
  let data = e.data
  console.log('handleError', data)
  if (data.Errcode === '1006') {
    // VueNot.$emit('logoOut')
    return false
  }
  if (data.Errcode === '1021') {
    let str = data.Errmsg + ''
    if (str == '帐号不存在') {
      // VueNot.$emit('logoOut')
      return false
    }
  }
  if (data.Errcode === '1041') {
    let str = data.Errmsg + ''
    if (str == '帐号已被拉黑:账户被禁用') {
      // VueNot.$emit('logoOut')
      return false
    }
  }


  return true
}
function checkisShowLoading (url) {
  if (url.indexOf('air_campustalk/increase/video_play_count'>=0)) {
    return false
  }
  return true
}

export default http

