
const appConfig = [
  {
    id: "0",
    title: "首页",
    icon: "el-icon-monitor",
    module: "home",
    defaultRegister: true,
    devEntry: "//localhost:2000/home'",
    depEntry: "http://www.zhangyingdong.com/home/",
    entry: "/home/",
    routerBase: "/home",
    children: [
      {
        id: "0-1",
        title: "首页中心",
        url: "/home"
      },
      {
        id: "0-2",
        title: "关于中心",
        url: "/home/about"
      }
    ]
  },
  {
    id: "1",
    title: "就业事物",
    icon: "el-icon-monitor",
    module: "vue3",
    defaultRegister: false,
    devEntry: "//localhost:7105",
    depEntry: "http://air.goworkla.cn/vue3/",
    entry:'/vue3/',
    routerBase: "/vue3",
    children: [
      {
        id: "1-1",
        title: "签约中心",
        url: "/vue3"
      },
      {
        id: "1-2",
        title: "档案管理",
        url: "/vue3/about"
      },
      {
        id: "1-3",
        title: "档案列表",
        url: "/vue3/workList"
      }
    ]
  },
  {
    id: "vue ts项目3",
    title: "就业指导",
    icon: "el-icon-date",
    module: "worker",
    defaultRegister: false,
    devEntry: "//localhost:2003/",
    depEntry: "http://air.goworkla.cn/worker/",
    entry:'/worker/',
    routerBase: "/worker",
    children: [
      {
        id: "2-1",
        title: "预约咨询",
        url: "/worker"
      },
      {
        id: "2-2",
        title: "教师管理",
        url: "/worker/about"
      },
      {
        id: "2-3",
        title: "讲座管理",
        url: "/worker/workerList"
      },

    ]
  },
  {
    id: "3",
    title: "系统设置",
    icon: "el-icon-monitor",
    module: "setting",
    defaultRegister: false,
    devEntry: "//localhost:2004/",
    depEntry: "http://air.goworkla.cn/setting/",
    entry:'/setting/',
    routerBase: "/setting",
    children: [
      {
        id: "3-1",
        title: "签约中心",
        url: "/setting"
      },
      {
        id: "3-2",
        title: "档案管理",
        url: "/setting/about"
      }
    ]
  },
  {
    id: "4",
    title: "college",
    icon: "el-icon-monitor",
    module: "college",
    defaultRegister: false,
    devEntry: "//localhost:6652/",
    depEntry: "http://air.goworkla.cn/college/",
    entry:'/college/',
    routerBase: "/college",
    children: [
      {
        id: "4-1",
        title: "签约中心",
        url: "/college"
      },
      {
        id: "4-2",
        title: "档案管理",
        url: "/college/about"
      }
    ]
  }
]

export default [
  {
    url: '/Api/GetAppConfigs',
    type: 'post',
    response: () => {
      return {
        code: 200,
        data: appConfig
      }
    }
  },
]
