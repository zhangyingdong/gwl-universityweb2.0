module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:vue/essential",
        "plugin:@typescript-eslint/eslint-recommended"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "parser": "@typescript-eslint/parser",
        "sourceType": "module"
    },
    "plugins": [
        "vue",
        "@typescript-eslint"
    ],
    "rules": {
      'no-console': 'off',
      'space-in-parens': 'off',
      'no-debugger': 'off',
      'no-mixed-operators': 'off',
      'eqeqeq': 'off',
      'no-unused-vars': 'off',
      'camelcase': 'off',
      // allow paren-less arrow functions
      'arrow-parens': 'off',
      // allow async-await
      'generator-star-spacing': 'off',
      // allow debugger during development
      'one-var': 'off',
      'indent': 'off',
      'vue/no-unused-components': 'off',
    }
};
