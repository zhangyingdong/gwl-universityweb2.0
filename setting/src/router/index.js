const routes = [
  { path: '/', name: 'home', component: () => import(/* webpackChunkName: "home" */ '@/views/Home') },
  { path: '/about', name: 'about', component: () => import(/* webpackChunkName: "about" */ '@/views/About') },
  { path: '/workList', name: 'workList', component: () => import(/* webpackChunkName: "about" */ '@/views/workList') },
  { path: '/detail', name: 'detail', component: () => import(/* webpackChunkName: "about" */ '@/views/detail') },


];

export default routes;
